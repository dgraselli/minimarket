# Minimarket

Backend API for Minimarket

## Design

This design is simple, just to fit request.

Adventages: 

* simplicity
* Voucher Logic extensible

Disadventages: 

* Open times is restricted to simple days of week and one time range by day.


## Database Model

Voucher logic is serializated on Voucher.serilized_data, because I think is not necesary for search. Only is possible to search by date.

![DER](./Doc/Der.png)

## VoucherProcess Class Model

This model represents Voucher logic.

A VoucherCommand, apply, Action, only if Rules are true

```
if (rule.Evaluate( context ))
   action.Apply( context )   

```

This design its based on *Command*, *Strategy* and *Composite* design patterns


![VoucherProcess Class Model](./Doc/VoucherProcess_ClassModel.png)



## Run project

Edit file **.env** and set the variable **ConnectionStrings__Default**

For example, this use default password on docker container database

> ConnectionStrings__Default="User ID=sa;password=coCa#2121;server=localhost;Database=minimarket_dev;" 

then

> cd Minimarket

> dotnet run


## Database on docker

Its posible, run a docker container with database. *(Docker is required)*

Edit file **./Docker/run_db.sh**, for set SA_PASSWORD, and next run:

> ./Docker/run_db.sh


## Resource Endpoints

* GET /api/[resource]
* GET /api/[resource]/id
* POST /api/[resource]
* PUT /api/[resource]/id
* DELETE  /api/[resource]/id

Where resources are:

* stores
* products
* cateogories
* carts
* vouchers


### Functionality

On agree with requirements, provides this endpoints

* Be able to setup all data from a simple GET
> /setup

* Be able to query available stores at a certain time in the day and return only those that
apply

> /storesOpenedAt

Example for current date

> /storesOpenedAt

Example for a particular date

> /storesOpenedAt?dt=2021-01-01 15:25


* Be able to query all available products, across stores, with their total stock.

> productsOfStore?storeName=?

Example

> productsOfStore?storeName=COCO Bay


* Be able to query if a product is available, at a certain store, and return that product's
info

> /existsOnStore?storeName=&productName=

Example 

> /existsOnStore?storeName=COCO Bay&productName=Diet Slurm

* Be able to query available products for a particular store

> /api/store/{storeId}/products

* Be able to manage a simple virtual cart (add/remove from it). It cannot allow to add a
product that has NO stock

> /addPoduct?cartId=[cartId]&productId=[]&quantity=[]

> /removePoduct?cartId=[cartId]&productId=[]&quantity=[]

* Be able to check the validity of a Voucher code on said virtual cart. Calculate discounts
and return both original and discounted prices

> /api/cart/[cartId]/applyVoucher/[voucherCode]


## Testing

Some changes should be done for raise unit test coverage. 

![Test Coverage](./Doc/testCoverage.png)
