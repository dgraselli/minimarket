using Microsoft.EntityFrameworkCore;
using Minimarket.Model;

namespace Minimarket.Data
{
    public class MinimarketContext : DbContext
    {
        public DbSet<Store>? stores { get; set; }
        public DbSet<Product>? products { get; set; }
        public DbSet<Category>? categories { get; set; }
        public DbSet<StoreProductStock>? storeProductStocks { get; set; }
        public DbSet<Cart>? carts { get; set; }
        public DbSet<Voucher>? vouchers { get; set; }


        public MinimarketContext(string cn) : base(GetOptions(cn))
        {
            this.Database.EnsureCreated();
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return new DbContextOptionsBuilder().UseSqlServer(connectionString).Options;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {         
            //modelBuilder.Entity<StoreProductStock>().Navigation(e => e.product).AutoInclude();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {

            //optionsBuilder.UseLazyLoadingProxies();

        }
    
    }
}