using Microsoft.EntityFrameworkCore;
using minimarket;
using Minimarket.Data;
using Minimarket.DataAccess;
using Minimarket.Model;
using Minimarket.BusinessLogic;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();



            
//Setup DBContext & DI
var cn = builder.Configuration.GetConnectionString("Default");
MinimarketConfig.Setup(cn, builder);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }