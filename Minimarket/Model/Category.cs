using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class Category : BaseEntity
    {
        public string name {get; set;} = "";
        [AdaptIgnore]
        public virtual List<Product> products {get; set;} = new List<Product>();

        public Category()
        {
        }
        public Category(string name)
        {
            this.name = name;
        }        
    }
}