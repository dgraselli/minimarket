using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]    
    public class CartProducts : BaseEntity
    {
        public int cartId {get; set;}
        public virtual Cart cart {get; set;} = null!;

        public int productId {get; set;}
        public virtual Product product {get;set;} = null!;

        public int quantity {get; set;}

        public decimal discount {get;set;}
        public string? discountDesc {get;set;}
        
        
    }
}