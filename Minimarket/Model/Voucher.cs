using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class Voucher : BaseEntity
    {
        public string? code {get; set;}
        public int storeId {get;set;}
        public Store store {get;set;} = null!;
        public DateTime? validFrom {get; set;}
        public DateTime? validTo {get; set;}

        [AdaptIgnore]
        public string? serialized_data {get; set;}

        public Voucher () {}
        public Voucher(string code, Store store, DateTime validFrom, DateTime validTo)
        {
            this.code = code;
            this.store = store;
            this.validFrom = validFrom;
            this.validTo = validTo;
        }

        //Return if date is beteen valid dates
        public bool IsValid() => this.IsValid(DateTime.Now);
        public bool IsValid(DateTime date)
        {
            if (
                (this.validFrom != null && date < this.validFrom)
                ||
                (this.validTo != null && date > this.validTo)
            )
            {
                return false;
            }

            return true;
        }
    }
}