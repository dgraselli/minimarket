using Mapster;
using Minimarket.Exceptions;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class Product : BaseEntity
    {
        public string name {get; set;} = "";
        
        public string? desc {get; set;}

        public decimal price {get; set;}

        public virtual List<Category> categories {get; set;} = new List<Category>();

        public Product()
        {}
        public Product(string name, decimal price = 0)
        {
            this.name = name;
            this.price = price;
            this.categories = new List<Category>();
        }

        public void addCategory(Category category)
        {
            this.categories.Add(category);
        }
        public void removeCategory(Category category)
        {
            if (this.categories.Count() == 1)
            {
                throw new EVisibleException($"Product '{this.name}' must has at leas one category");
            }
            this.categories.Add(category);
        }
        

    }
}