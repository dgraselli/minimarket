using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class StoreProductStock : BaseEntity
    {
        public int storeId { get; set; }
        public virtual Store store { get; set; } = null!;
        public int productId { get; set; }
        public virtual Product product { get; set; } = null!;

        public int stock { get; set; }

        public StoreProductStock()
        {

        }
        public StoreProductStock(Store store, Product product, int stock)
        {
            this.store = store;
            this.product = product;
            this.stock = stock;
        }

        public override string ToString()
        {
            return "Stock: " + store + " " + product;
        }

    }
}