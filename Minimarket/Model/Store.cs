using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class Store : BaseEntity
    {
        public string? name { get; set; }

        [MaxLength(7)]
        public string openedDays { get; set; } = "";

        public int fromHour { get; set; }
        public int toHour { get; set; }

        [AdaptIgnore]
        public virtual List<StoreProductStock> products { get; set; } = new List<StoreProductStock>();

        public Store()
        {
        }
        
        public Store(string name)
        {
            this.name = name;
        }

        //public Store(string name, string openedDays ="", int fromHour=0, int toHour=24)
        public Store(string name, string openedDays, int fromHour, int toHour)
        {
            this.name = name;
            this.openedDays = openedDays;
            this.fromHour = fromHour;
            this.toHour = toHour;
        }

        public bool isOpened()
        {
            return this.isOpened(DateTime.Now);
        }

        public bool isOpened(DateTime dt)
        {
            int dayOfWeek = (int)dt.DayOfWeek;
            if (!this.openedDays.Contains(dayOfWeek.ToString()))
                return false;

            return dt.Hour >= this.fromHour && dt.Hour < this.toHour;
        }
    }
}