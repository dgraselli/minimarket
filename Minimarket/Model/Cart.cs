using Mapster;

namespace Minimarket.Model
{
    [AdaptTo("[name]Dto"), GenerateMapper]
    public class Cart : BaseEntity
    {
        public int storeId {get; set;}
        public virtual Store? store {get; set;}
        public DateTime dt {get; set;} = DateTime.Now;
        public virtual List<CartProducts> products {get; set;} = new List<CartProducts>();
    
        public Cart()
        {

        }

        public override string ToString()
        {
            return $"Cart({id}, store: {storeId})";
        }
    }
}