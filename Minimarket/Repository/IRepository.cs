using Microsoft.EntityFrameworkCore;
using Minimarket.Model;

namespace Minimarket.DataAccess
{
    public interface IRepository<T> where T : BaseEntity
    {
        DbContext getContext();
        DbSet<T> GetDbSet();
        IList<T> GetAll();
        Task<IList<T>> GetAllAsync();

        T? GetById(int id);
        Task<T?> GetByIdAsync(int id);

        void Add(T enity);
        Task AddAsync(T enity);
        void AddRange(T[] enity);
        Task AddRangeAsync(T[] enity);
        void Update(T enity);
        Task UpdateAsync(T enity);
        T? Remove(int id);
        void RemoveAll();
        void Save();
        Task SaveAsync();

        IQueryable<T> GetQuery();
        Task<IQueryable<T>> GetQueryAsyc();
    }
}