using Microsoft.EntityFrameworkCore;
using Minimarket.Data;
using Minimarket.Model;

namespace Minimarket.DataAccess
{
    
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected DbContext context;
        protected DbSet<T> dbSet;

        public Repository(MinimarketContext _context)
        {
            context = _context;
            this.dbSet = this.context.Set<T>();
        }

        public DbSet<T> GetDbSet()
        {
            return dbSet;
        }
        public DbContext getContext()
        {
            return context;
        }

        public IList<T> GetAll()
        {
            IQueryable<T> query = this.dbSet;
            return query.ToList();
        }
        public async Task<IList<T>> GetAllAsync()
        {
            IQueryable<T> query = this.dbSet;
            return await query.ToListAsync();
        }

        public T? GetById(int id)
        {
            return dbSet.Find(id);
        }
        public async Task<T?> GetByIdAsync(int id)
        {
            var t = await dbSet.FindAsync(id);
            return t;
        }

        public void Add(T enity)
        {
            this.dbSet.Add(enity);
            context.SaveChanges();
        }

        public async Task AddAsync(T enity)
        {
            await this.dbSet.AddAsync(enity);
            await context.SaveChangesAsync();
        }


        public void AddRange(T[] enity)
        {
            this.dbSet.AddRange(enity);
            context.SaveChanges();
        }

        public async Task AddRangeAsync(T[] enity)
        {
            await this.dbSet.AddRangeAsync(enity);
            await context.SaveChangesAsync();
        }

        public void Update(T enity)
        {
            context.Entry(enity).State = EntityState.Modified;
            context.SaveChanges();
        }
        public async Task UpdateAsync(T enity)
        {
            context.Entry(enity).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }
        public T? Remove(int id)
        {
            T? entity = dbSet.Find(id);
            if (entity != null)
            {
                dbSet.Remove(entity);
                context.SaveChanges();
            }
            return entity;
        }

        public void RemoveAll()
        {
            dbSet.RemoveRange(dbSet);
        }


        public void Save()
        {
            context.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public IQueryable<T> GetQuery()
        {
            IQueryable<T> query = dbSet;
            return query;
        }

        public async Task<IQueryable<T>> GetQueryAsyc()
        {
            var query = await dbSet.ToListAsync();
            return query.AsQueryable();
        }
    }

    
}