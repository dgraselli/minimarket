using Microsoft.EntityFrameworkCore;
using Minimarket.Data;
using Minimarket.DataAccess;
using Minimarket.Model;
using Minimarket.BusinessLogic;
using Serilog;
using System.Text.Json.Serialization;
using Microsoft.OpenApi.Models;
using System.Reflection;
using MapsterMapper;
using Mapster;

namespace minimarket
{
    public static class MinimarketConfig
    {
        public static void Setup(string connectionString, WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<MinimarketContext>(_ => new MinimarketContext(connectionString));
            builder.Services.AddTransient<IApplicationBL, ApplicationBL>();
            
            //Avoid infinite cycle error on json
            //builder.Services.AddControllers().AddJsonOptions(x =>
            //    x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);
            
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;

            //Entities
            builder.Services.AddTransient<IRepository<Store>, Repository<Store>>();
            builder.Services.AddTransient<IRepository<Product>, Repository<Product>>();
            builder.Services.AddTransient<IRepository<Category>, Repository<Category>>();
            builder.Services.AddTransient<IRepository<StoreProductStock>, Repository<StoreProductStock>>();
            builder.Services.AddTransient<IRepository<Cart>, Repository<Cart>>();
            builder.Services.AddTransient<IRepository<CartProducts>, Repository<CartProducts>>();
            builder.Services.AddTransient<IRepository<Voucher>, Repository<Voucher>>();

            //Add DBContext
            builder.Services.AddDbContext<Minimarket.Data.MinimarketContext>(
                options =>
                {
                    options.UseSqlServer(connectionString);
                }
            );

            //Swagger DOC
            setSwaggerConfig(builder);
           
            //Mapster
            setMapsterConfig(builder);

            //Config Logger
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .CreateLogger();
        
            //Json config
            //TODO: 
        }

        private static void setMapsterConfig(WebApplicationBuilder builder)
        {
            var config = new TypeAdapterConfig();
            config.NewConfig<CartProducts, CartProductsDto>().PreserveReference(false);

            builder.Services.AddSingleton(config);
            builder.Services.AddScoped<IMapper, ServiceMapper>();
        }

        private static void setSwaggerConfig(WebApplicationBuilder builder)
        {

            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "ToDo API",
                    Description = "An ASP.NET Core Web API for managing ToDo items",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Example Contact",
                        Url = new Uri("https://example.com/contact")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Example License",
                        Url = new Uri("https://example.com/license")
                    }
                });

                // using System.Reflection;
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });
            
        }
    }
}