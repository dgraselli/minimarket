namespace Minimarket.Exceptions
{
    public class ContextPropertyException : EVisibleException
    {
        private string? propertyName;
        private Type? type;

        public ContextPropertyException(string propertyName, Type? type) : base()
        {
            this.propertyName = propertyName;
            this.type = type;
        }
        public override string Message => $"Property {propertyName} must be of type {type}";
    }
}