namespace Minimarket.Exceptions
{
    public class EVisibleException : Exception
    {
        public EVisibleException() :  base()
        {

        }
        public EVisibleException(string msg) :  base(msg)
        {

        }
    }
}