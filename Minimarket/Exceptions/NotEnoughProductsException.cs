namespace Minimarket.Exceptions
{
    public class NotEnoughProductsException : EVisibleException
    {
        public int productId;
        public NotEnoughProductsException(int productId) : base()
        {
            this.productId = productId;
        }

        public override string Message => $"Not enough products";
    }
}