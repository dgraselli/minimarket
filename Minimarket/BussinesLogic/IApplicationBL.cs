using Minimarket.DataAccess;
using Minimarket.Model;

namespace Minimarket.BusinessLogic
{
    public interface IApplicationBL 
    {
     
        void Setup();

        // Return only stores opened at that time
        IList<StoreDto> GetStoresOpenedAt(DateTime datetime);

        // Return products with stock > 0 of store: storeId 
        List<StoreProductStockDto> getProductsWithStockOfStore(string storeName);

        StoreProductStockDto getProductsOfStore(string storeName, string productName);

        // Add product q to store cart, only if product has stock on that store
        Cart AddProductToCart(int cartId, int productId, int quantity);
        
        // Remove prouct q from store cart, only if it has
        Cart RemoveProductToCart(int cartId, int productId, int quantity);

        CartDto TestVoucher(int cartId, string voucherCode, DateTime dt);

        // Check Voucher exists and valid time
        void ValidateVoucher(string voucherCode, DateTime datetime);
 
   }
}