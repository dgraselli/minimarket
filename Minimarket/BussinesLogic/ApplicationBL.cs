using Minimarket.Model;
using Minimarket.DataAccess;
using Serilog;
using Minimarket.Exceptions;
using Minimarket.BussinesLogic.VoucherLogic;
using System.Text.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Minimarket.Data;

namespace Minimarket.BusinessLogic
{
    public class ApplicationBL : IApplicationBL
    {
        MinimarketContext context;
        private readonly IRepository<Store> storeRepo;
        private readonly IRepository<Product> productRepo;
        private readonly IRepository<Category> categoryRepo;
        private readonly IRepository<StoreProductStock> stockRepo;
        private readonly IRepository<Cart> cartRepo;
        private readonly IRepository<CartProducts> cartProductsRepo;
        private readonly IRepository<Voucher> voucherRepo;

        public ApplicationBL(
            MinimarketContext context,
            IRepository<Store> storeRepo,
            IRepository<Product> productRepo,
            IRepository<Category> categoryRepo,
            IRepository<StoreProductStock> stockRepo,
            IRepository<Cart> cartRepo,
            IRepository<Voucher> voucherRepo,
            IRepository<CartProducts> cartProductsRepo
            )
        {
            this.context = context;
            this.storeRepo = storeRepo; //  ?? throw new ArgumentNullException(nameof(storeRepo));;
            this.productRepo = productRepo;
            this.categoryRepo = categoryRepo;
            this.stockRepo = stockRepo;
            this.cartRepo = cartRepo;
            this.voucherRepo = voucherRepo;
            this.cartProductsRepo = cartProductsRepo;
        }

        public void Setup()
        {
            // Remove ALL ---------------------------------------------------
            storeRepo.RemoveAll();
            categoryRepo.RemoveAll();
            productRepo.RemoveAll();
            stockRepo.RemoveAll();
            cartRepo.RemoveAll();
            voucherRepo.RemoveAll();

            //Add Stores ----------------------------------------------------
            var s_coco_downtown = new Store("COCO Downtown", "1", 9, 22);
            var s_coco_bay = new Store("COCO Bay", "123456", 7, 18);
            var s_coco_mall = new Store("COCO Mall", "0123456", 0, 24);
            storeRepo.Add(s_coco_downtown);
            storeRepo.Add(s_coco_bay);
            storeRepo.Add(s_coco_mall);
            storeRepo.Save();

            //Add Categories ------------------------------------------------
            var c_soda = new Category("Sodas");
            var c_food = new Category("Food");
            var c_cleaning = new Category("Cleaning");
            var c_bathroom = new Category("Bathrom");
            categoryRepo.AddRange(new[] { c_soda, c_food, c_cleaning, c_bathroom });

            //Add Products --------------------------------------------------
            c_soda.products.AddRange(new[] {
                new Product("Cold Ice Tea", 1.1m),
                new Product("Coffee flavoured milk", 2.1m),
                new Product("Nuke-Cola", 3.10m),
                new Product("Sprute", 4.1m),
                new Product("Slurm", 5.1m),
                new Product("Diet Slurm", 6.1m)
            });
            c_food.products.AddRange(new[] {
                new Product("Salsa Cookies", 7.2m),
                new Product("Windmill Cookies", 8.2m),
                new Product("Garlic-o-bread 2000", 9.2m),
                new Product("LACTEL bread", 10.2m),
                new Product("Ravioloches x12", 11.2m),
                new Product("Ravioloches x48", 12.2m),
                new Product("Milanga ganga", 12.2m),
                new Product("Milanga ganga napo", 13.2m)
            });
            c_cleaning.products.AddRange(new[] {
                new Product("Atlantis detergent", 14.3m),
                new Product("Virulanita", 15.3m),
                new Product("Sponge, Bob", 16.3m),
                new Product("Generic mop", 17.3m)
            });

            c_bathroom.products.AddRange(new[] {
                new Product("Pure steel toilet paper", 18.4m),
                new Product("Generic soap", 19.4m),
                new Product("PANTONE shampoo", 20.4m),
                new Product("Cabbagegate toothpaste", 21.4m)
            });
            categoryRepo.Save();

            //Add Stocks ----------------------------------------
            var s_coco_downtown_absent = new[]{
                "Sprute",
                "Slurm",
                "Atlantis detergent",
                "Virulanita",
                "Sponge, Bob",
                "Generic mop",
                "Pure steel toilet paper"
            };
            var s_coco_bay_absent = new[] {
                "Diet Slurm",
                "PANTONE shampoo",
                "Pure steel toilet paper",
                "Generic soap",
                "Cabbagegate toothpaste"
            };

            var s_coco_mall_absent = new[] {
                "Ravioloches x12",
                "Ravioloches x48",
                "Milanga ganga",
                "Milanga ganga napo",
                "Atlantis detergent",
                "Virulanita",
                "Sponge, Bob",
                "Generic mop",
            };

            foreach (Store s in storeRepo.GetAll())
            {
                foreach (Product p in productRepo.GetAll())
                {
                    var sp_stock = new StoreProductStock(s,p,100);
                    stockRepo.Add(sp_stock);
                }
            }

            // Set Absent prducts  -----------------------
            foreach (string prod in s_coco_bay_absent)
            {
                foreach (var stock in stockRepo.GetQuery().Where(s => s.product.name == prod && s.store == s_coco_bay))
                {
                    stock.stock = 0;
                }
            }
            foreach (string prod in s_coco_downtown_absent)
            {
                foreach (var stock in stockRepo.GetQuery().Where(s => s.product.name == prod && s.store == s_coco_downtown))
                {
                    stock.stock = 0;
                }
            }
            foreach (string prod in s_coco_mall_absent)
            {
                foreach (var stock in stockRepo.GetQuery().Where(s => s.product.name == prod && s.store == s_coco_mall))
                {
                    stock.stock = 0;
                }
            }
            storeRepo.Save();


            // Add Vouchers ---------------------------------------------------------

            //Voucher 1, 20% Wdn & Thu on Cleaning 
            voucherRepo.Add(getVoucher_1(s_coco_bay));
            //Voucher 2, Take 3 Pay 2 on Windmill Cookies ---------
            voucherRepo.Add(getVoucher_2(s_coco_bay));
            //Voucher 3, 10% Off on Bathroom & Sodas  
            voucherRepo.Add(getVoucher_3(s_coco_mall));
            //Voucher 4, 13% on 2th unit on "Nuka-Cola", "Slurm" and "Diet Slurm"
            voucherRepo.Add(getVoucher_4(s_coco_downtown));
            //Voucher 5, 50% on 2th unit, on "Hang-yourself toothpaste", only on Mondays
            voucherRepo.Add(getVoucher_5(s_coco_downtown));

        }

        public IList<StoreDto> GetStoresOpenedAt(DateTime datetime)
        {
            return storeRepo.GetAll().Where(s => s.isOpened(datetime)).Select(s => s.AdaptToDto()).ToList();
        }

        public void ValidateVoucher(string voucherCode, DateTime datetime)
        {
            var voucher = voucherRepo.GetQuery().Where(v => v.code == voucherCode).SingleOrDefault();
            if (voucher == null)
            {
                throw new EVisibleException("Invalid voucher code");
            }

            if (!voucher.IsValid(datetime))
            {
                throw new EVisibleException("Invalid expired");
            }
        }
        public CartDto TestVoucher(int cartId, string voucherCode, DateTime dt)
        {

            var cart = cartRepo.GetQuery()
                .Include(c => c.store)
                .Include(c => c.products)
                .ThenInclude(cp => cp.product)
                .ThenInclude(p => p.categories)
                .Where(c => c.id == cartId).SingleOrDefault();

            if (cart == null)
            {
                throw new EVisibleException($"Cart doesn't exists");
            }

            var voucher = voucherRepo.GetQuery().Where(v => v.code == voucherCode).SingleOrDefault();
            if (voucher == null)
            {
                throw new EVisibleException($"Voucher with code {voucherCode} doesn't exists");
            }
            if (!voucher.IsValid(dt))
            {
                throw new EVisibleException($"Voucher with code {voucherCode} is out of date");
            }


            VoucherCommand vc  = (VoucherCommand) deserialize(voucher.serialized_data!);
            var cartDTO = cart.AdaptToDto();
            foreach(var cp in cartDTO.products)
            {
                cp.cart = cartDTO;
                vc.Process(cp);
            }
            

            return cartDTO;

        }

        private Voucher getVoucher_1(Store store)
        {
            var rule_days_wdn_thu = new DayOfWeekRule("34");
            var rule_cleaning = new IsInCategoriesRule(new[] { "Cleaning" });
            var action_discount_20 = new PercentDiscountAction(20);

            var v = new Voucher(
                "COCO1V1F8XOG1MZZ",
                store,
                DateTime.Parse("2022-01-27"),
                DateTime.Parse("2022-02-13"));
            var vp_rule = new CompositeAllRule(new BaseRule[] { rule_days_wdn_thu, rule_cleaning });
            var vp_action = action_discount_20;
            var vp = new VoucherCommand(vp_action, vp_rule);
            v.serialized_data = this.serialize(vp);

            return v;
        }

        private Voucher getVoucher_2(Store s_coco_bay)
        {
            var rule_windmill_cookies = new IsInProductsRule(new[] { "Windmill Cookies" });
            var action_take3pay2 = new TakeNPayMAction(3, 2, 6);

            var v = new Voucher(
                "COCOKCUD0Z9LUKBN:",
                s_coco_bay,
                DateTime.Parse("2022-01-24"),
                DateTime.Parse("2022-02-06"));
            var vp_rule = rule_windmill_cookies;
            var vp_action = action_take3pay2;
            var vp = new VoucherCommand(vp_action, vp_rule);
            v.serialized_data = this.serialize(vp);
            return v;
        }

        private Voucher getVoucher_3(Store s_coco_mall)
        {
            var rule_sodas_bath = new IsInCategoriesRule(new[] { "Sodas", "Bathrom" });
            var action_discount_10 = new PercentDiscountAction(10);

            var v = new Voucher(
                "COCOG730CNSG8ZVX:",
                s_coco_mall,
                DateTime.Parse("2022-01-31"),
                DateTime.Parse("2022-02-09"));
            var vp_rule = rule_sodas_bath;
            var vp_action = action_discount_10;
            var vp = new VoucherCommand(vp_action, vp_rule);
            v.serialized_data = this.serialize(vp);
            return v;
        }

        private Voucher getVoucher_4(Store s_coco_downtown)
        {
            var rule_nuka_slurm = new IsInProductsRule(new[] { "Nuke-Cola", "Slurm", "Diet Slurm" });
            var action_discount_30_on_2th_unit = new DiscountOnNextUnitAction(30, 2);

            var v = new Voucher(
                "COCO2O1USLC6QR22",
                s_coco_downtown,
                DateTime.Parse("2022-02-01"),
                DateTime.Parse("2022-02-28"));
            var vp_rule = rule_nuka_slurm;
            var vp_action = action_discount_30_on_2th_unit;
            var vp = new VoucherCommand(vp_action, vp_rule);
            v.serialized_data = this.serialize(vp);
            return v;
        }
        private Voucher getVoucher_5(Store s_coco_downtown)
        {
            var rule_days_mon = new DayOfWeekRule("1");
            var rule_hang_yourself = new IsInProductsRule(new[] { "Hang-yourself toothpaste" });
            //FIX: product "Hang-yourself toothpaste" doesn't exist
            var action_discount_50_on_2th_unit = new DiscountOnNextUnitAction(50, 2);

            var v = new Voucher(
                "COCO2O1USLC6QR22",
                s_coco_downtown,
                DateTime.Parse("2022-02-01"),
                DateTime.Parse("2022-02-14"));
            var vp_rule = new CompositeAllRule(new BaseRule[] { rule_days_mon, rule_hang_yourself });
            var vp_action = action_discount_50_on_2th_unit;
            var vp = new VoucherCommand(vp_action, vp_rule);
            v.serialized_data = this.serialize(vp);
            return v;
        }

        private string? serialize(VoucherCommand vp1)
        {
            SoapFormatter formatter = new SoapFormatter();
            MemoryStream ms = new MemoryStream();
            formatter.Serialize(ms, vp1);
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        private object deserialize(string data)
        {
            SoapFormatter formatter = new SoapFormatter();
            MemoryStream ms = new MemoryStream( Encoding.ASCII.GetBytes(data));
            return formatter.Deserialize(ms);
        }


        private CartProducts getCartProduct(Product product, Cart cart)
        {
            //search current product or create it            
            var cart_product = cartProductsRepo.GetQuery().Where(
                cp => cp.productId == product.id
                &&
                cp.cartId == cart.id
                ).SingleOrDefault();

            if (cart_product == null)
            {
                cart_product = new CartProducts();
                cart_product.cart = cart;
                cart_product.product = product;
                cartProductsRepo.Add(cart_product);
            }

            return cart_product;

        }
        public Cart AddProductToCart(int cartId,int productId, int quantity)
        {
            var product = productRepo.GetById(productId);
            if(product == null) throw new EVisibleException("product doesn't exists");
            var cart = cartRepo.GetQuery().Include(s=>s.products).Where(c => c.id == cartId).SingleOrDefault();
            if(cart == null) throw new EVisibleException("cart doesn't exists");


            var store = storeRepo.GetById(cart.storeId);
            if (store == null) throw new Exception($"Store doesn't exists: {cart.storeId}");

            var ps = stockRepo.GetQuery().Where(sp => sp.productId == product.id && sp.storeId == store.id).Single();

            //validate stock is enough
            if (ps.stock < quantity)
            {
                throw new NotEnoughProductsException(product.id);
            }

            var cart_product = getCartProduct(product, cart);
            // add quantity to cart
            cart_product.quantity += quantity;

            // discount stock
            ps.stock -= quantity;
            cartRepo.Save();

            return cart;
        }

        public Cart RemoveProductToCart(int cartId, int productId, int quantity)
        {
            var product = productRepo.GetById(productId);
            if(product == null) throw new EVisibleException("product doesn't exists");
            var cart = cartRepo.GetQuery().Include(s=>s.products).Where(c => c.id == cartId).SingleOrDefault();
            if(cart == null) throw new EVisibleException("cart doesn't exists");

            var store = storeRepo.GetById(cart.storeId);
            if (store == null) throw new EVisibleException($"Store doesn't exists: {cart.storeId}");

            var ps = stockRepo.GetQuery().Where(sp => sp.productId == product.id && sp.storeId == store.id).Single();

            var cart_product = getCartProduct(product, cart);

            //validate enough
            if (cart_product.quantity < quantity)
            {
                throw new NotEnoughProductsException(cart_product.productId);
            }

            // add quantity to cart
            cart_product.quantity -= quantity;

            // if no product left, delete it
            if (cart_product.quantity == 0)
            {
                cartProductsRepo.Remove(cart_product.id);
            }

            // discount stock
            ps.stock += quantity;
            
            cartRepo.Save();
            return cart;
        }

        public StoreProductStockDto getProductsOfStore(string storeName, string productName)
        {
            var store = storeRepo.GetQuery().Where(s => s.name == storeName).SingleOrDefault();
            if (store == null) throw new EVisibleException($"Store doesn't exists: {storeName}");
            var product = productRepo.GetQuery().Where(p => p.name == productName).SingleOrDefault();
            if (product == null) throw new EVisibleException($"Product doesn't exists: {productName}");

            var stock = stockRepo.GetQuery().Where(s=>s.product == product && s.store==store).SingleOrDefault();
            if (stock == null) throw new EVisibleException($"Product {productName} doesn't exists on store {storeName}");

            return stock.AdaptToDto();
        }
        public List<StoreProductStockDto> getProductsWithStockOfStore(string storeName)
        {
            var store = storeRepo.GetQuery()
               .Include(s => s.products)
               .Where(s => s.name == storeName).SingleOrDefault();
            if (store == null) throw new EVisibleException($"Store doesn't exists: {storeName}");

            var list = new List<StoreProductStockDto>();

            return stockRepo.GetQuery().Where(s=> s.store == store && s.stock > 0).Select(s => s.AdaptToDto()).ToList();
        }

        public IRepository<Category> getCategoryRepo(){
            return categoryRepo;
        }
    }
}