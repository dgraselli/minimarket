namespace Minimarket.BussinesLogic.VoucherLogic
{
    public interface IRule
    {
        bool Evaluate(IContext ctx);
    }
}