using Minimarket.Exceptions;

namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class BaseRule : IRule
    {
        public virtual bool Evaluate(IContext ctx)
        {
            throw new NotImplementedException("Must be implemented by subclass !");
        }

        protected virtual object getContextProperty(IContext ctx, string propertyName, Type t)
        {
            var prop = ctx.getProperty(propertyName);
            if (prop.GetType() != t)
            {
                throw new ContextPropertyException(propertyName, t);
            }
            return prop; 
        }
        
    }
}