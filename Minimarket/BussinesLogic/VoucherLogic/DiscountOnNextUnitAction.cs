namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class DiscountOnNextUnitAction : BaseAction
    {
        public int percentDiscount {get;set;} = 0;
        public int unit {get;set;}

        public DiscountOnNextUnitAction(int percent, int unit = 2)
        {
            this.percentDiscount = percent;
            this.unit = unit;
        }
        public override void Apply(IContext ctx)
        {
            //get context properties
            var quantity = (int) getContextProperty(ctx, "quantity", typeof(int));
            var price = (decimal) getContextProperty(ctx, "price", typeof(decimal));

            //calc discount and set description
            decimal discount = this.getDiscount(quantity, price);
            var discountDesc = $"Discount {this.percentDiscount}% on {unit}th unit";
            
            //set context results
            ctx.setResult("discount", discount);
            ctx.setResult("discountDesc", discountDesc);
        }

        // Get amount to discount based on quantity and price
        private decimal getDiscount(int quantity, decimal price)
        {
            int units_to_discount = this.getUnitsToDiscount(quantity);
            decimal unit_price = price / quantity;
            return units_to_discount * unit_price * percentDiscount;
        }

        // Get total units to discount.
        // For example, 
        //    if is discount on 2th unit then:
        //       quantity=2, untis_with_discount=1
        //       quantity=3, untis_with_discount=1
        //       quantity=4, untis_with_discount=2
        //       quantity=5, untis_with_discount=2
        // 
        public int getUnitsToDiscount(int quantity)
        {
            return (quantity - (quantity % unit)) / unit;
        }
    }
}