namespace Minimarket.BussinesLogic.VoucherLogic
{
    // Rule to applay on certain products 
    [Serializable]
    public class IsInListRule : BaseRule
    {
        public string[] list {get; set;}

        public virtual bool Validate(string item)
        {
            return this.list.Contains(item);
        }


        public IsInListRule(string[] list)
        {
            this.list = list;
        }
    }
}