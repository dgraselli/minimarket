namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class PercentDiscountAction : BaseAction
    {
        public int percentDiscount {get;set;} = 0;

        public PercentDiscountAction(int percent)
        {
            this.percentDiscount = percent;
        }
        public override void Apply(IContext ctx)
        {
            //get context  var
            var price = (decimal) getContextProperty(ctx, "price", typeof(decimal));

            //calc discount
            var discount = this.getDiscount(price);
            var discountDesc = $"Discount {this.percentDiscount}%";
            
            //set context results
            ctx.setResult("discount", discount);
            ctx.setResult("discountDesc", discountDesc);
        }

        public decimal getDiscount(decimal price)
        {
            return price * this.percentDiscount / 100;
        }
    }
}