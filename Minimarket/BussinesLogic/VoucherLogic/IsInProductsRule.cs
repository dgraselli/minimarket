namespace Minimarket.BussinesLogic.VoucherLogic
{
    // Rule to applay on certain products 
    [Serializable]
    public class IsInProductsRule : IsInListRule
    {
        public override bool Evaluate(IContext ctx)
        {
            var productName = (string) getContextProperty(ctx, "productName", typeof(string));
            return this.Validate(productName!);
        }
        public IsInProductsRule(string[] products) : base(products){}
    }
}