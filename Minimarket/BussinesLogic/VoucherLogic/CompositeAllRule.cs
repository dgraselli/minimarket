namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class CompositeAllRule : BaseRule
    {
        public BaseRule[] rules {get; set;} = new BaseRule[]{};

        public override bool Evaluate(IContext ctx)
        {
            return rules.All(r => r.Evaluate(ctx));
        }

        public CompositeAllRule(BaseRule[] rules)
        {
            this.rules = rules;
        }
        
    }
}