using Minimarket.Model;

namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class VoucherCommand
    {
        public BaseRule? rule {get;set;}
        public BaseAction action {get;set;}

        public void Process(IContext ctx)
        {
            if (rule == null || rule.Evaluate(ctx))
            {
                action.Apply(ctx);
            }
        }

        public VoucherCommand(BaseAction action, BaseRule? rule = null)
        {
            this.action = action;
            this.rule = rule;
        }
    }
}