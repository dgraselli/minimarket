using Minimarket.Exceptions;

namespace Minimarket.BussinesLogic.VoucherLogic
{

    // Rule to apply one or more days a week
    [Serializable]
    public class DayOfWeekRule : BaseRule
    {
        public string validDays {get;set;} = "";

        public override bool Evaluate(IContext ctx)
        {
            var date = (DateTime) getContextProperty(ctx, "date", typeof(DateTime));
            return this.Validate(date);
        }

        public bool Validate(DateTime date)
        {
            return this.validDays.Contains(((int)date.DayOfWeek).ToString() );
        }

        public DayOfWeekRule(string validDays)
        {
            this.validDays = validDays;
        }
    }
}