namespace Minimarket.BussinesLogic.VoucherLogic
{
    public interface IContext
    {
        object getProperty(string propertyName);
        void setResult(string propertyName, object value);
    }
}