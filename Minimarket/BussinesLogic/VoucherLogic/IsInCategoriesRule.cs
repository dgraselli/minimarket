namespace Minimarket.BussinesLogic.VoucherLogic
{
    // Rule to applay on certain products 
    [Serializable]
    public class IsInCategoriesRule : IsInListRule
    {
        public override bool Evaluate(IContext ctx)
        {
            var categories = (string[]) getContextProperty(ctx, "categories", typeof(string[]));

            foreach(var category in categories)
            {
                if ( this.Validate(category) ) 
                {
                    return true;
                }
            }
            return false;
        }

        public IsInCategoriesRule(string[] categories) : base(categories) {}
    }
}