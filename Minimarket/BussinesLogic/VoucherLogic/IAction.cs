namespace Minimarket.BussinesLogic.VoucherLogic
{
    public interface IAction
    {
        void Apply(IContext ctx);
    }
}