using Minimarket.Exceptions;

namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class BaseAction : IAction
    {
        public virtual void Apply(IContext ctx)
        {

        }

        protected virtual object getContextProperty(IContext ctx, string propertyName, Type t)
        {
            var prop = ctx.getProperty(propertyName);
            if (prop.GetType() != t)
            {
                throw new ContextPropertyException(propertyName, t);
            }
            return prop; 
        }        
    }
}