namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class CompositeAnyRule : BaseRule
    {
        public List<BaseRule> rules {get; set;} = new List<BaseRule>();

        public override bool Evaluate(IContext ctx)
        {
            return rules.Any(r => r.Evaluate(ctx));
        }
        
    }
}