namespace Minimarket.BussinesLogic.VoucherLogic
{
    [Serializable]
    public class TakeNPayMAction : BaseAction
    {
        public int take {get; set;}
        public int pay {get; set;}
        public int limit {get; set;}
        
        public TakeNPayMAction(int take, int pay, int limit=int.MaxValue)
        {
            this.take = take;
            this.pay = pay;
            this.limit = limit;
        }

        public override void Apply(IContext ctx)
        {
            //get context  var
            var quantity = (int) getContextProperty(ctx, "quantity", typeof(int));
            var price = (decimal) getContextProperty(ctx, "price", typeof(decimal));

            //if buy less than take, return 
            if (quantity < this.take) 
            {
                return;
            }

            //apply limit
            var val_to_discount = Math.Min(quantity, this.limit);
            
            //rest module
            val_to_discount -= val_to_discount % take;

            //calc factor
            var factor = (val_to_discount ) / take * pay;


            var discount = price * factor;
            var discountDesc = $"Discount {take}x{pay} (-{val_to_discount} units)";

            //set context results
            ctx.setResult("discount", discount);
            ctx.setResult("discountDesc", discountDesc);
        }
    }
}