using Microsoft.AspNetCore.Mvc;
using Minimarket.Controllers;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;

namespace Minimarket.Controllers
{
    [ApiController]
    [Route("api/[controller]s")]
    public class StoreController : GenericController<Store, StoreDto>
    {
        public StoreController(IRepository<Store> repo) : base(repo)
        {
        }
    }
}