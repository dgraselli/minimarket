using System.Net;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Mvc;
using Minimarket.DataAccess;
using Minimarket.Exceptions;
using Minimarket.Model;
using Serilog;


namespace Minimarket.Controllers
{
    public class GenericController<T,TDto> : Controller where T : BaseEntity
    {
        protected readonly IRepository<T> _repo;

        public GenericController(IRepository<T> repo)
        {
            _repo = repo;
        }

        private TDto toDTO(T o)
        {
            return TypeAdapter.Adapt<TDto>(o);
        }

        /// <summary>
        /// Get all Resources
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<ActionResult> Get()
        {
            try
            {
                var ret = await _repo.GetAllAsync();
                return Ok(ret.ToArray().Select(s=>toDTO(s)));
            }
            catch(EVisibleException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        /// <summary>
        /// Get Resource by ID
        /// </summary>
        /// <parameter>id</parameter>
        /// <returns>Resource</returns>
        [HttpGet]
        [Route("{id}")]
        public virtual async Task<ActionResult<T>>? Get(int id)
        {
            try
            {
                var o = await _repo.GetByIdAsync(id);
                if(o != null) return Ok(toDTO(o));
                else return NotFound("doesn't exists");
            }
            catch(EVisibleException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        /// <summary>
        /// Create Resource
        /// </summary>
        /// <parameter>Resource</parameter>
        /// <returns>Resource</returns>
        [HttpPost]
        public virtual async Task<ActionResult<T>?> Create(T o)
        {
            try
            {
                await _repo.AddAsync(o);
                return Created(Url.Page("") ?? "", toDTO(o));
            }
            catch(EVisibleException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        /// <summary>
        /// Update Resource by ID
        /// </summary>
        /// <parameter>id</parameter>
        /// <parameter>Resource</parameter>
        /// <returns>Resource</returns>
        [HttpPut]
        [Route("{id}")]
        public virtual async Task<ActionResult<T>?> Update(T o)
        {
            try
            {
                await _repo.UpdateAsync(o);
                return Accepted(this.Url.Page("") ?? "", toDTO(o));
            }
            catch(EVisibleException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }


        /// <summary>
        /// Delete Resource by ID
        /// </summary>
        /// <parameter>id</parameter>
        /// <returns>Resource</returns>
        [HttpDelete]
        [Route("{id}")]
        public virtual ActionResult<T>? Delete(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest("Invalid id");

                var o =_repo.Remove(id);
                return this.Ok(toDTO(o!));
            }
            catch(EVisibleException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}