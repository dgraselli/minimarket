using Microsoft.AspNetCore.Mvc;
using Minimarket.Controllers;
using Minimarket.DataAccess;
using Minimarket.Model;

namespace Minimarket.Controllers
{
    [ApiController]
    [Route("api/[controller]s")]
    public class ProductController : GenericController<Product, ProductDto>
    {
        private IRepository<StoreProductStock> _repoStock;
        private IRepository<Product> _repoProduct;

        public ProductController(
            IRepository<Product> repo,
            IRepository<StoreProductStock> repoStock,
            IRepository<Product> repoProduct
            ) : base(repo)
        {
            this._repoStock = repoStock;
            this._repoProduct = repoProduct;
        }

    }
}