using Microsoft.AspNetCore.Mvc;
using Minimarket.Controllers;
using Minimarket.DataAccess;
using Minimarket.Model;

namespace Minimarket.Controllers
{
    [ApiController]
    [Route("api/categories")]
    public class CategoryController : GenericController<Category, CategoryDto>
    {

        public CategoryController(IRepository<Category> repo) : base(repo)
        {
        }
    }
}