using Microsoft.AspNetCore.Mvc;
using Minimarket.DataAccess;
using Minimarket.Model;

namespace Minimarket.Controllers
{
    [ApiController]
    [Route("api/[controller]s")]
    public class CartController : GenericController<Cart, CartDto>
    {
        public CartController(IRepository<Cart> repo) : base(repo)
        {
        }
    
    }
}