using Microsoft.AspNetCore.Mvc;
using Minimarket.Model;
using Minimarket.BusinessLogic;
using Serilog;
using Minimarket.Exceptions;

namespace Minimarket.Controllers
{
    [ApiController]
    public class MainController : Controller
    {
        private readonly IApplicationBL _applicationBL;

        public MainController(IApplicationBL applicationBL)
        {
            _applicationBL = applicationBL;
        }

        /// <summary>
        /// Setup project database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("setup")]
        public string Setup()
        {
            _applicationBL.Setup();
            return "Setup Ok!";
        }

        [HttpGet]
        [Route("storesOpenedAt")]
        public ActionResult<List<StoreDto>> OpenedStores(string? dt)
        {
            try
            {
                if (dt == null)
                {
                    dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                Log.Debug($"Datetime is :{dt}");

                var datetime = DateTime.Parse(dt);
                return Ok(_applicationBL.GetStoresOpenedAt(datetime));
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost]
        [Route("addProduct")]
        public ActionResult<CartDto> addProduct(int cartId, int productId, int quantity)
        {
            try
            {
                var cart = _applicationBL.AddProductToCart(cartId, productId, quantity);
                return Ok(cart.AdaptToDto());
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        [Route("removeProduct")]
        public ActionResult<CartDto> removeProduct(int cartId, int productId, int quantity=1)
        {
            try
            {
                var cart = _applicationBL.RemoveProductToCart(cartId, productId, quantity);
                return Ok(cart.AdaptToDto());
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet]
        [Route("validateVoucher")]
        public ActionResult Validate(string voucherCode, string dt)
        {
            try
            {
                if (dt == null)
                {
                    dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                Log.Debug($"Datetime is :{dt}");

                var datetime = DateTime.Parse(dt);

                _applicationBL.ValidateVoucher(voucherCode, datetime);
                return Ok("ok");
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet]
        [Route("testVoucher")]
        public ActionResult testVoucher(int cartId, string voucherCode, string? dt)
        {
            try
            {
                if (dt == null)
                {
                    dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                Log.Debug($"Datetime is :{dt}");

                var datetime = DateTime.Parse(dt);

                return Ok(_applicationBL.TestVoucher(cartId, voucherCode, datetime));
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }

        }

        [HttpGet]
        [Route("productsOfStore")]
        public ActionResult getProductsOfStore(string storeName)
        {
            try{
                return Ok(_applicationBL.getProductsWithStockOfStore(storeName));
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpGet]
        [Route("existsOnStore")]
        public ActionResult<StoreProductStockDto> existsOnStore(string storeName, string productName)
        {
            try
            {
                return Ok(_applicationBL.getProductsOfStore(storeName, productName));
            }
            catch(EVisibleException e) 
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                Log.Error(e, $"ERROR: {Request.PathBase}");
                return StatusCode(500, "Internal server error");
            }
        }


        /***
        [HttpGet]
        [Route("productsOfStorePaginated/{storeId}")]
        public PageDTO<ProductStockDTO> getProductsPaginated(int storeId, int page = 1, int page_count = 10)
        {
            var r = new PageDTO<ProductStockDTO>();

            var list = new List<ProductStockDTO>();

            var query = (from sp in stockre.GetQuery().Where(sp => sp.stock > 0)
                //join s in _repo.GetQuery() on sp.storeId equals s.id
                join p in _repoProduct.GetQuery() on sp.productId equals p.id
                select new
                {
                    id = sp.productId,
                    product = p.name,
                    stock = sp.stock,
                    price = p.price
                });
          
            var row_from = page_count * (page - 1);
            foreach (var p in query.Skip(row_from).Take(page_count).ToList())
            {
                list.Add(new ProductStockDTO(p.product, p.stock, p.price));
            }

            r.data = list;
            r.from = row_from;
            r.to = row_from + page_count;
            r.count = query.Count();
            //Thread.Sleep(1000);
            return r;
            }
    }
    */

    }
}