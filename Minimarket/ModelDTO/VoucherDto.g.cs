using System;
using Minimarket.Model;

namespace Minimarket.Model
{
    public partial class VoucherDto
    {
        public string? code { get; set; }
        public int storeId { get; set; }
        public StoreDto store { get; set; }
        public DateTime? validFrom { get; set; }
        public DateTime? validTo { get; set; }
        public int id { get; set; }
    }
}