using System.Collections.Generic;
using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class CartMapper
    {
        public static CartDto AdaptToDto(this Cart p1)
        {
            return p1 == null ? null : new CartDto()
            {
                storeId = p1.storeId,
                store = p1.store == null ? null : new StoreDto()
                {
                    name = p1.store.name,
                    openedDays = p1.store.openedDays,
                    fromHour = p1.store.fromHour,
                    toHour = p1.store.toHour,
                    id = p1.store.id
                },
                dt = p1.dt,
                products = funcMain1(p1.products),
                id = p1.id
            };
        }
        public static CartDto AdaptTo(this Cart p6, CartDto p7)
        {
            if (p6 == null)
            {
                return null;
            }
            CartDto result = p7 ?? new CartDto();
            
            result.storeId = p6.storeId;
            result.store = funcMain5(p6.store, result.store);
            result.dt = p6.dt;
            result.products = funcMain6(p6.products, result.products);
            result.id = p6.id;
            return result;
            
        }
        
        private static List<CartProductsDto> funcMain1(List<CartProducts> p2)
        {
            if (p2 == null)
            {
                return null;
            }
            List<CartProductsDto> result = new List<CartProductsDto>(p2.Count);
            
            int i = 0;
            int len = p2.Count;
            
            while (i < len)
            {
                CartProducts item = p2[i];
                result.Add(funcMain2(item));
                i++;
            }
            return result;
            
        }
        
        private static StoreDto funcMain5(Store p8, StoreDto p9)
        {
            if (p8 == null)
            {
                return null;
            }
            StoreDto result = p9 ?? new StoreDto();
            
            result.name = p8.name;
            result.openedDays = p8.openedDays;
            result.fromHour = p8.fromHour;
            result.toHour = p8.toHour;
            result.id = p8.id;
            return result;
            
        }
        
        private static List<CartProductsDto> funcMain6(List<CartProducts> p10, List<CartProductsDto> p11)
        {
            if (p10 == null)
            {
                return null;
            }
            List<CartProductsDto> result = new List<CartProductsDto>(p10.Count);
            
            int i = 0;
            int len = p10.Count;
            
            while (i < len)
            {
                CartProducts item = p10[i];
                result.Add(funcMain7(item));
                i++;
            }
            return result;
            
        }
        
        private static CartProductsDto funcMain2(CartProducts p3)
        {
            return p3 == null ? null : new CartProductsDto()
            {
                cartId = p3.cartId,
                productId = p3.productId,
                product = funcMain3(p3.product),
                quantity = p3.quantity,
                discount = p3.discount,
                discountDesc = p3.discountDesc,
                id = p3.id
            };
        }
        
        private static CartProductsDto funcMain7(CartProducts p12)
        {
            return p12 == null ? null : new CartProductsDto()
            {
                cartId = p12.cartId,
                productId = p12.productId,
                product = funcMain8(p12.product),
                quantity = p12.quantity,
                discount = p12.discount,
                discountDesc = p12.discountDesc,
                id = p12.id
            };
        }
        
        private static ProductDto funcMain3(Product p4)
        {
            return p4 == null ? null : new ProductDto()
            {
                name = p4.name,
                desc = p4.desc,
                price = p4.price,
                categories = funcMain4(p4.categories),
                id = p4.id
            };
        }
        
        private static ProductDto funcMain8(Product p13)
        {
            return p13 == null ? null : new ProductDto()
            {
                name = p13.name,
                desc = p13.desc,
                price = p13.price,
                categories = funcMain9(p13.categories),
                id = p13.id
            };
        }
        
        private static List<CategoryDto> funcMain4(List<Category> p5)
        {
            if (p5 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p5.Count);
            
            int i = 0;
            int len = p5.Count;
            
            while (i < len)
            {
                Category item = p5[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
        
        private static List<CategoryDto> funcMain9(List<Category> p14)
        {
            if (p14 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p14.Count);
            
            int i = 0;
            int len = p14.Count;
            
            while (i < len)
            {
                Category item = p14[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
    }
}