using System.Collections.Generic;
using System.Text.Json.Serialization;
using Minimarket.Model;

namespace Minimarket.Model
{
    public partial class ProductDto
    {
        public string name { get; set; }
        [JsonIgnore]
        public string? desc { get; set; }
        public decimal price { get; set; }
        [JsonIgnore]
        public List<CategoryDto> categories { get; set; }
        public int id { get; set; }
    }
}