using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Minimarket.Model;

namespace Minimarket.Model
{
    public partial class CartDto
    {
        public int storeId { get; set; }
        [JsonIgnore]
        public StoreDto? store { get; set; }
        public DateTime dt { get; set; }
        public List<CartProductsDto> products { get; set; }
        public int id { get; set; }
    }
}