using System.Collections.Generic;
using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class ProductMapper
    {
        public static ProductDto AdaptToDto(this Product p1)
        {
            return p1 == null ? null : new ProductDto()
            {
                name = p1.name,
                desc = p1.desc,
                price = p1.price,
                categories = funcMain1(p1.categories),
                id = p1.id
            };
        }
        public static ProductDto AdaptTo(this Product p3, ProductDto p4)
        {
            if (p3 == null)
            {
                return null;
            }
            ProductDto result = p4 ?? new ProductDto();
            
            result.name = p3.name;
            result.desc = p3.desc;
            result.price = p3.price;
            result.categories = funcMain2(p3.categories, result.categories);
            result.id = p3.id;
            return result;
            
        }
        
        private static List<CategoryDto> funcMain1(List<Category> p2)
        {
            if (p2 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p2.Count);
            
            int i = 0;
            int len = p2.Count;
            
            while (i < len)
            {
                Category item = p2[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
        
        private static List<CategoryDto> funcMain2(List<Category> p5, List<CategoryDto> p6)
        {
            if (p5 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p5.Count);
            
            int i = 0;
            int len = p5.Count;
            
            while (i < len)
            {
                Category item = p5[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
    }
}