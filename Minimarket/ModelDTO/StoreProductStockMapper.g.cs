using System.Collections.Generic;
using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class StoreProductStockMapper
    {
        public static StoreProductStockDto AdaptToDto(this StoreProductStock p1)
        {
            return p1 == null ? null : new StoreProductStockDto()
            {
                storeId = p1.storeId,
                store = p1.store == null ? null : new StoreDto()
                {
                    name = p1.store.name,
                    openedDays = p1.store.openedDays,
                    fromHour = p1.store.fromHour,
                    toHour = p1.store.toHour,
                    id = p1.store.id
                },
                productId = p1.productId,
                product = funcMain1(p1.product),
                stock = p1.stock,
                id = p1.id
            };
        }
        public static StoreProductStockDto AdaptTo(this StoreProductStock p4, StoreProductStockDto p5)
        {
            if (p4 == null)
            {
                return null;
            }
            StoreProductStockDto result = p5 ?? new StoreProductStockDto();
            
            result.storeId = p4.storeId;
            result.store = funcMain3(p4.store, result.store);
            result.productId = p4.productId;
            result.product = funcMain4(p4.product, result.product);
            result.stock = p4.stock;
            result.id = p4.id;
            return result;
            
        }
        
        private static ProductDto funcMain1(Product p2)
        {
            return p2 == null ? null : new ProductDto()
            {
                name = p2.name,
                desc = p2.desc,
                price = p2.price,
                categories = funcMain2(p2.categories),
                id = p2.id
            };
        }
        
        private static StoreDto funcMain3(Store p6, StoreDto p7)
        {
            if (p6 == null)
            {
                return null;
            }
            StoreDto result = p7 ?? new StoreDto();
            
            result.name = p6.name;
            result.openedDays = p6.openedDays;
            result.fromHour = p6.fromHour;
            result.toHour = p6.toHour;
            result.id = p6.id;
            return result;
            
        }
        
        private static ProductDto funcMain4(Product p8, ProductDto p9)
        {
            if (p8 == null)
            {
                return null;
            }
            ProductDto result = p9 ?? new ProductDto();
            
            result.name = p8.name;
            result.desc = p8.desc;
            result.price = p8.price;
            result.categories = funcMain5(p8.categories, result.categories);
            result.id = p8.id;
            return result;
            
        }
        
        private static List<CategoryDto> funcMain2(List<Category> p3)
        {
            if (p3 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p3.Count);
            
            int i = 0;
            int len = p3.Count;
            
            while (i < len)
            {
                Category item = p3[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
        
        private static List<CategoryDto> funcMain5(List<Category> p10, List<CategoryDto> p11)
        {
            if (p10 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p10.Count);
            
            int i = 0;
            int len = p10.Count;
            
            while (i < len)
            {
                Category item = p10[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
    }
}