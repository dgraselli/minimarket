using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class CategoryMapper
    {
        public static CategoryDto AdaptToDto(this Category p1)
        {
            return p1 == null ? null : new CategoryDto()
            {
                name = p1.name,
                id = p1.id
            };
        }
        public static CategoryDto AdaptTo(this Category p2, CategoryDto p3)
        {
            if (p2 == null)
            {
                return null;
            }
            CategoryDto result = p3 ?? new CategoryDto();
            
            result.name = p2.name;
            result.id = p2.id;
            return result;
            
        }
    }
}