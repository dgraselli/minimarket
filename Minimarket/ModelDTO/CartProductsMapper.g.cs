using System.Collections.Generic;
using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class CartProductsMapper
    {
        public static CartProductsDto AdaptToDto(this CartProducts p1)
        {
            return p1 == null ? null : new CartProductsDto()
            {
                cartId = p1.cartId,
                productId = p1.productId,
                product = funcMain1(p1.product),
                quantity = p1.quantity,
                discount = p1.discount,
                discountDesc = p1.discountDesc,
                id = p1.id
            };
        }
        public static CartProductsDto AdaptTo(this CartProducts p4, CartProductsDto p5)
        {
            if (p4 == null)
            {
                return null;
            }
            CartProductsDto result = p5 ?? new CartProductsDto();
            
            result.cartId = p4.cartId;
            result.productId = p4.productId;
            result.product = funcMain3(p4.product, result.product);
            result.quantity = p4.quantity;
            result.discount = p4.discount;
            result.discountDesc = p4.discountDesc;
            result.id = p4.id;
            return result;
            
        }
        
        private static ProductDto funcMain1(Product p2)
        {
            return p2 == null ? null : new ProductDto()
            {
                name = p2.name,
                desc = p2.desc,
                price = p2.price,
                categories = funcMain2(p2.categories),
                id = p2.id
            };
        }
        
        private static ProductDto funcMain3(Product p6, ProductDto p7)
        {
            if (p6 == null)
            {
                return null;
            }
            ProductDto result = p7 ?? new ProductDto();
            
            result.name = p6.name;
            result.desc = p6.desc;
            result.price = p6.price;
            result.categories = funcMain4(p6.categories, result.categories);
            result.id = p6.id;
            return result;
            
        }
        
        private static List<CategoryDto> funcMain2(List<Category> p3)
        {
            if (p3 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p3.Count);
            
            int i = 0;
            int len = p3.Count;
            
            while (i < len)
            {
                Category item = p3[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
        
        private static List<CategoryDto> funcMain4(List<Category> p8, List<CategoryDto> p9)
        {
            if (p8 == null)
            {
                return null;
            }
            List<CategoryDto> result = new List<CategoryDto>(p8.Count);
            
            int i = 0;
            int len = p8.Count;
            
            while (i < len)
            {
                Category item = p8[i];
                result.Add(item == null ? null : new CategoryDto()
                {
                    name = item.name,
                    id = item.id
                });
                i++;
            }
            return result;
            
        }
    }
}