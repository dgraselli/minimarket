using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class StoreMapper
    {
        public static StoreDto AdaptToDto(this Store p1)
        {
            return p1 == null ? null : new StoreDto()
            {
                name = p1.name,
                openedDays = p1.openedDays,
                fromHour = p1.fromHour,
                toHour = p1.toHour,
                id = p1.id
            };
        }
        public static StoreDto AdaptTo(this Store p2, StoreDto p3)
        {
            if (p2 == null)
            {
                return null;
            }
            StoreDto result = p3 ?? new StoreDto();
            
            result.name = p2.name;
            result.openedDays = p2.openedDays;
            result.fromHour = p2.fromHour;
            result.toHour = p2.toHour;
            result.id = p2.id;
            return result;
            
        }
    }
}