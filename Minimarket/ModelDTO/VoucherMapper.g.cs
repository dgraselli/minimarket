using Minimarket.Model;

namespace Minimarket.Model
{
    public static partial class VoucherMapper
    {
        public static VoucherDto AdaptToDto(this Voucher p1)
        {
            return p1 == null ? null : new VoucherDto()
            {
                code = p1.code,
                storeId = p1.storeId,
                store = new StoreDto()
                {
                    name = p1.store.name,
                    openedDays = p1.store.openedDays,
                    fromHour = p1.store.fromHour,
                    toHour = p1.store.toHour,
                    id = p1.store.id
                },
                validFrom = p1.validFrom,
                validTo = p1.validTo,
                id = p1.id
            };
        }
        public static VoucherDto AdaptTo(this Voucher p2, VoucherDto p3)
        {
            if (p2 == null)
            {
                return null;
            }
            VoucherDto result = p3 ?? new VoucherDto();
            
            result.code = p2.code;
            result.storeId = p2.storeId;
            result.store = funcMain1(p2.store, result.store);
            result.validFrom = p2.validFrom;
            result.validTo = p2.validTo;
            result.id = p2.id;
            return result;
            
        }
        
        private static StoreDto funcMain1(Store p4, StoreDto p5)
        {
            if (p4 == null)
            {
                return null;
            }
            StoreDto result = p5 ?? new StoreDto();
            
            result.name = p4.name;
            result.openedDays = p4.openedDays;
            result.fromHour = p4.fromHour;
            result.toHour = p4.toHour;
            result.id = p4.id;
            return result;
            
        }
    }
}