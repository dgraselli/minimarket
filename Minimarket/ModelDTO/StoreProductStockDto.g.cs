using Minimarket.Model;

namespace Minimarket.Model
{
    public partial class StoreProductStockDto
    {
        public int storeId { get; set; }
        public StoreDto store { get; set; }
        public int productId { get; set; }
        public ProductDto product { get; set; }
        public int stock { get; set; }
        public int id { get; set; }
    }
}