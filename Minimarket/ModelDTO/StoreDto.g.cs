namespace Minimarket.Model
{
    public partial class StoreDto
    {
        public string? name { get; set; }
        public string openedDays { get; set; }
        public int fromHour { get; set; }
        public int toHour { get; set; }
        public int id { get; set; }
    }
}