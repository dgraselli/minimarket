using System.Text.Json.Serialization;
using Minimarket.BussinesLogic.VoucherLogic;
using Minimarket.Exceptions;
using Minimarket.Model;

namespace Minimarket.Model
{
    public partial class CartProductsDto : IContext
    {
        [JsonIgnore]
        public int cartId { get; set; }
        [JsonIgnore]
        public CartDto cart { get; set; }
        public int productId { get; set; }
        public ProductDto product { get; set; }
        public int quantity { get; set; }
        public decimal discount { get; set; }
        public string? discountDesc { get; set; }
        public int id { get; set; }
        public object getProperty(string propertyName)
        {
            switch(propertyName)
            {
                case  "date":
                    return this.cart.dt!;
                case  "quantity":
                    return this.quantity;
                case  "productName":
                    return this.product.name;
                case  "categories":
                    return this.product.categories.Select(s => s.name).ToArray();
                case  "price":
                    return this.product.price * this.quantity;

                default:
                    throw new EVisibleException($"Property {propertyName}, doesn't exists on {this.GetType().ToString()}");

            }
        }

        public void setResult(string propertyName, object value)
        {
            switch(propertyName)
            {
                case "discount":
                    discount = (decimal) value;
                    break;
                case "discountDesc":
                    discountDesc = (string) value;
                    break;
                default:
                    throw new EVisibleException($"Property {propertyName}, doesn't exists on {this.GetType().ToString()}");
            }
        }    
    }
}