using Xunit;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;
using System;
using Minimarket.BussinesLogic.VoucherLogic;

namespace Minimarket.Tests;

public class TestVoucherRules
{

    [Theory]
    [InlineData("0","2022-05-01")]
    [InlineData("1","2022-05-02")]
    [InlineData("2","2022-05-03")]
    [InlineData("3","2022-05-04")]
    [InlineData("4","2022-05-05")]
    [InlineData("5","2022-05-06")]
    [InlineData("6","2022-05-07")]
    [InlineData("0123456","2022-05-07")]
    [InlineData("0123456","2023-05-07")]
    [InlineData("0123456","2024-05-07")]
    [InlineData("0123456","2025-05-07")]
    [InlineData("0123456","2026-05-07")]
    public void TestDayOfWeekRule_Valids(string days, string date_str)
    {
        var rule = new DayOfWeekRule(days);
        var dt = DateTime.Parse(date_str);
        Assert.True(rule.Validate(dt));
    }

    [Theory]
    [InlineData("0","2022-05-02")]
    [InlineData("1","2022-05-03")]
    [InlineData("2","2022-05-04")]
    [InlineData("3","2022-05-05")]
    [InlineData("4","2022-05-06")]
    [InlineData("5","2022-05-07")]
    [InlineData("6","2022-05-08")]
    [InlineData("123456","2022-05-01")]
    [InlineData("023456","2022-05-02")]
    [InlineData("013456","2022-05-03")]
    [InlineData("012456","2022-05-04")]
    [InlineData("012356","2022-05-05")]
    [InlineData("012346","2022-05-06")]
    [InlineData("012345","2022-05-07")]
    public void TestDayOfWeekRule_InValids(string days, string date_str)
    {
        var rule = new DayOfWeekRule(days);
        var dt = DateTime.Parse(date_str);
        Assert.False(rule.Validate(dt));
    }

}
