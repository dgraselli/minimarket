using Xunit;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;
using System;

namespace Minimarket.Tests;

public class TestStore
{

    [Fact]
    public void TestIsOpened()
    {
        var o = new Store("Test", "12345", 0, 22);

        var dt_in_arr = new DateTime[] {
            new System.DateTime(2022, 5, 5, 9, 22, 0),
            new System.DateTime(2022, 5, 3, 12, 22, 0)
            };

        var dt_out_arr = new DateTime[] {
            new System.DateTime(2022, 5, 5, 23, 22, 0),
            new System.DateTime(2022, 5, 8, 10, 22, 0)
            };

        
        foreach (var d in dt_in_arr) {
            Assert.True(o.isOpened(d),
                     $"Should be opened: {d}");
        } 

        foreach (var d in dt_out_arr) {
            Assert.False(o.isOpened(d),
                     $"Should be closed: {d}");
        } 

    }
}