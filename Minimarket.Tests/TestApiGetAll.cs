using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Minimarket.Tests
{
    public class TestApiGetAll : IClassFixture<ApiWebAppFactory>
    {
        readonly HttpClient _client;

        public TestApiGetAll(ApiWebAppFactory application)
        {
            _client = application.CreateClient();
        }

        [Theory]
        //[InlineData("setup")]
        [InlineData("storesOpenedAt")]
        [InlineData("api/stores")]
        [InlineData("api/categories")]
        [InlineData("api/products")]
        [InlineData("api/carts")]
        [InlineData("api/vouchers")]
        public async Task GetAll(string resource)
        {
            var response = await _client.GetAsync($"/{resource}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }


    }
}