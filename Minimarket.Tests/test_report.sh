
rm -rf TestResults
rm -rf TestReport

dotnet test --collect:"XPlat Code Coverage"

FILE=`find . -iname *cobertura*`

reportgenerator \
-reports:"$FILE" \
-targetdir:"TestReport" \
-reporttypes:Html 

firefox ./TestReport/index.html
