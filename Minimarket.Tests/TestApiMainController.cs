using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Minimarket.Tests
{

    public class TestApiMainController : IClassFixture<ApiWebAppFactory>
    {
        readonly HttpClient _client;

        public TestApiMainController(ApiWebAppFactory application)
        {
            _client = application.CreateClient();
        }

        [Theory]
        [InlineData("2022-05-01 08:15:01")]
        [InlineData("2020-02-12 08:25:59")]
        [InlineData("2020-12-31 18:15:01")]
        public async Task GET_openedStoresAt(string dt)
        {
            var response = await _client.GetAsync($"/storesOpenedAt?dt={dt}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("adsfasdf")]
        [InlineData("2020-02-30")]
        [InlineData("2020-02-30 25:01:01")]
        public async Task GET_openedStoresAt_Error(string dt)
        {
            var response = await _client.GetAsync($"/storesOpenedAt?dt={dt}");
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }


        [Theory]
        [InlineData("COCO Downtown")]
        [InlineData("COCO Bay")]
        [InlineData("COCO Mall")]
        public async Task productsOfStore(string storeName)
        {
            var response = await _client.GetAsync($"/productsOfStore?storeName={storeName}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("N1")]
        [InlineData("N2")]
        public async Task productsOfInexistentStore(string storeName)
        {
            var response = await _client.GetAsync($"/productsOfStore?storeName={storeName}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

    }
}

