using Xunit;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;
using System;
using Minimarket.BusinessLogic;

namespace Minimarket.Tests;

public class TestMappers
{

    [Fact]
    public void testMaps()
    {
        var store = new Store();
        Assert.IsType<StoreDto>(store.AdaptToDto());

        var product = new Product();
        Assert.IsType<ProductDto>(product.AdaptToDto());

        var cat = new Category();
        Assert.IsType<CategoryDto>(cat.AdaptToDto());

        var cart = new Cart();
        Assert.IsType<CartDto>(cart.AdaptToDto());

        var voucher = new Voucher();
        Assert.IsType<VoucherDto>(voucher.AdaptToDto());

        var cartProd = new CartProducts();
        Assert.IsType<CartProductsDto>(cartProd.AdaptToDto());

        var stock = new StoreProductStock();
        Assert.IsType<StoreProductStockDto>(stock.AdaptToDto());

    }
}