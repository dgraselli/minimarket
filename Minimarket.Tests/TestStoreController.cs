using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Minimarket.Controllers;
using Minimarket.DataAccess;
using Minimarket.Model;
using Moq;
using Xunit;

namespace Minimarket.Tests
{
    public class TestStoreController
    {
        private readonly Mock<IRepository<Store>> mockRepo;

        public TestStoreController()
        {
            mockRepo = new Mock<IRepository<Store>>();
        }

        [Fact (Skip = "falta")]
        public async Task get()
        {
            mockRepo.Setup(x => x.GetAllAsync())
              .ReturnsAsync(GetSampleStores());

            var controller = new StoreController(mockRepo.Object);

            var result = (OkObjectResult) await controller.Get();
            var viewResult = Assert.IsType<OkObjectResult>(result);
            var model = (Store[]) Assert.IsAssignableFrom<IEnumerable<Store>>(viewResult.Value);
            Assert.Equal(GetSampleStores().Count, model.Length);
        }

        
        private Store GetSampleStore(int id)
        {
            return new Store
            {
                id = 1,
                name = "Store 1",
            };
        }
        private List<Store> GetSampleStores()
        {
            return new List<Store>
            {
                new Store
                {
                    id = 1,
                    name = "Store 1",
                },
                new Store
                {
                    id = 2,
                    name = "Store 2",
                }
            };

        }

        // private List<StoreDto> GetSampleStores()
        // {
        //     var list = new List<Store>
        //     {
        //         new Store
        //         {
        //             id = 1,
        //             name = "Store 1",
        //         },
        //         new Store
        //         {
        //             id = 2,
        //             name = "Store 2",
        //         }
        //     };
        //     return (from a in list select a.AdaptToDto()).ToList();
        // }
    }
}
