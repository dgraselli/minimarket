using Xunit;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;
using System;
using Minimarket.BussinesLogic.VoucherLogic;

namespace Minimarket.Tests;

public class TestVoucherActions
{

    [Theory]
    [InlineData(10)]
    [InlineData(99)]
    [InlineData(0)]
    public void TestPercentDiscountAction(int percent)
    {
        var action = new PercentDiscountAction(percent);
        var some_values = new decimal[] {22.10m, 199.12m, 315.5m, 1215.15m, 66, 1300000};
        
        foreach(var value in some_values)
        {
            Assert.Equal(value * percent / 100, action.getDiscount(value));
        }        
    }


    [Theory]
    [InlineData(2,0,0)]
    [InlineData(2,1,0)]
    [InlineData(2,2,1)]
    [InlineData(2,3,1)]
    [InlineData(2,4,2)]
    [InlineData(2,14,7)]
    [InlineData(2,15,7)]
    [InlineData(2,601,300)]
    [InlineData(3,0,0)]
    [InlineData(3,1,0)]
    [InlineData(3,2,0)]
    [InlineData(3,3,1)]
    [InlineData(3,4,1)]
    [InlineData(3,14,4)]
    [InlineData(3,15,5)]
    [InlineData(3,601,200)]    
    public void TestDiscountOnNextUnitAction_UnitsToDiscount(int unit, int quantity, int expected)
    {
        var action = new DiscountOnNextUnitAction(10, unit);
        Assert.Equal( expected, action.getUnitsToDiscount(quantity));
    }


}