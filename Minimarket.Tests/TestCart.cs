using Xunit;
using Minimarket.DataAccess;
using Minimarket.Model;
using Serilog;
using System;
using Minimarket.BusinessLogic;

namespace Minimarket.Tests;

public class TestCart
{
    private IApplicationBL applicationBL;
    public TestCart(IApplicationBL applicationBL)
    {
        this.applicationBL = applicationBL;
    }

    [Fact (Skip =  "falta")]
    public void TestNotEnoughProductException()
    {
        var prodWithStock = new Product("Prod with stock",10);
        var prodWithoutStock = new Product("Prod without stock",20);

        var store = new Store();
        store.products.Add(new StoreProductStock(store, prodWithStock, 10));
        store.products.Add(new StoreProductStock(store, prodWithoutStock, 0));
        Cart c = new Cart();            

    }
}