using System.Net.Http;
using System.Threading.Tasks;
using Minimarket.Model;
using Xunit;

namespace Minimarket.Tests
{

public class TestApiSetup: IClassFixture<ApiWebAppFactory>
{
    readonly HttpClient _client;

    public TestApiSetup(ApiWebAppFactory application)
    {
        _client = application.CreateClient();
    }

    [Fact  (Skip = "omit setup")]
    public async Task GET_setup()
    {
        var response = await _client.GetAsync("/setup");
        Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
    }



}
}

